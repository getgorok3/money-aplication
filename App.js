import * as React from 'react';
import 'react-native-gesture-handler'
import Home from './src/components/home'
import { AppLoading } from 'expo';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { store } from './src/system/store'
import { Provider } from 'react-redux';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

const AppNavigator = createStackNavigator({
  Home: {
    screen: Home,
  },
  
}, {
  initialRouteName: 'Home',
  defaultNavigationOptions: {
    headerStyle: {
      display: 'none'
    },
  }
}
);
const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }
  UNSAFE_componentWillMount() {
    this.loadFonts();
  }
   loadFonts = async () => {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }
  
  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>)

  }
}

