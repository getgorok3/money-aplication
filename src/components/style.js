import React from 'react'

const rootColor = {
    nero: '#1A1A1A', //black -- status canceled
    blue: '#3895D3', //blue -- main color -- status draft
    snow: '#FFFAFA',
    bottomLine: '#F1F3F6',
    white: '#FFFFFF',
    yellow: '#FFCC11'
}

export const styles = {

    // _________________________________ BUTTON _________________________________ //
    buttonMainContainer: {display: 'flex', maxWidth: '150px',margin: '0 auto'},
    buttonSubContainer: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5
    },
    buttonText: {textAlign: 'center'},
    // _________________________________ HEADER _________________________________ //
    headerBackground: {
        
        borderBottomWidth: '0.9px',
        borderBottomColor:  rootColor.bottomLine,
       
    },
    headerText: {
        color: rootColor.nero
    },
    headerIcon: {
        color: rootColor.nero
    },
    // circle:{
    //     width: '125px',
    //     height: '100px',
    //     position: 'absolute',
    //     borderRadius: '50%',
    //     backgroundColor: rootColor.snow
    // },
    // _________________________________ CONTAINER _________________________________ //
    flexCenter:{ flex: 1, alignItems: 'center', justifyContent: 'center',width: '100%' },
    contenBackground: { 
        backgroundColor: rootColor.white,
    },
    noneBackGround: { 
        backgroundColor: 'none',
    },
    // _________________________________ Tabs _________________________________ //
    tabStyles: { 
        backgroundColor: rootColor.white,
    },
    activeTabStyles: { 
        backgroundColor: rootColor.snow,
    },
    activeTextStyle: { 
        color: rootColor.blue,
        fontWeight: 'normal'
    },

    // __________________________________   TEXT   _________________________________ //
    mainText: {
        fontSize: '30px'
    },
    titleText: {
        fontSize: '18px',
        color: rootColor.white
    },

    // __________________________________   Animetion   _________________________________ //
    colorfulGradient: {
        // backgroundImage: "linear-gradient(to right, #eecda3   , #ef629f)",
        backgroundImage: "linear-gradient(to right, #a96f44    , #f2ecb6)",
       
    },
  


}