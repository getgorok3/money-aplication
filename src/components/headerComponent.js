import React from 'react';
import { View , Image} from 'react-native';
import {  Header, Title,  Button, Left, Right, Body, Icon, Text } from 'native-base';
import {styles} from './style'
import { images } from "../utilities/index";
export default function  HeaderComp0nent  () {
    return (
        <Header style={{...styles.headerBackground}}  transparent hasTabs>
         
          <Body style={{flexDirection: 'row',paddingLeft: '10px'}}>
            <Title style={{...styles.headerText}}>Money G</Title>
            <Image source={images.bees} resizeMod='contain'  style={{width: 30, height: 30}}/>
          </Body>
          <Right />
        
        </Header>
     
      )
}