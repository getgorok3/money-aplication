import React from 'react';
import { View,  TouchableOpacity,ImageBackground  } from 'react-native';
import Wallet from "../components/walletsList";
import { Container, Tabs,Tab, Content, Card, CardItem, Button, Text,Thumbnail,Left,Body,Image } from 'native-base';
import HeaderComp0nent from './headerComponent'
import {styles} from './style'
import MyFooter from './footer'
import Summary from './summary'
import {images} from '../utilities/index'
export default function  Home() {
    return (
      <Container>
        <HeaderComp0nent />
      
        <Content  style={{...styles.contenBackground,}} padder>
        
          <View style={{display: 'flex'}}>
          <Summary/>
          </View>
          <View style={{display: 'flex', flexDirection: 'row',justifyContent: 'space-around'}}>
          <Text>Income</Text>
          <Text>Expend</Text>
          </View>
          
        </Content>
        
        <MyFooter/>
      </Container>
    )
}




