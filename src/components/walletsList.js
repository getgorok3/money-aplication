import React from "react";
import { View,  TouchableOpacity } from 'react-native';
import { useDispatch, useSelector,shallowEqual } from "react-redux";
import {  Button, Icon, Text } from 'native-base';
import {styles} from './style'
// useSelector = mapStateToProps where to get redux state
// useDispatch = mapDispatchToProps all actions functions
export default function  Wallet  () {
  const wallets = useSelector(({wallet}) => wallet,[])
  const dispatch = useDispatch()

  return (
    <View >
      <Text style={{...styles.mainText}}>Wallets</Text>
      
      <View style={{...styles.flexCenter}}>
        <Button dark style={{ ...styles.buttonMainContainer,...styles.buttonSubContainer }}>
            <Text style={{...styles.buttonText}}> Dark </Text>
        </Button>
      </View>
    </View>
  )
}
