import React from 'react';
import { Tabs,Tab, Text } from 'native-base';
import {styles} from './style'

export default function  MyTabs  () {

    return (
         <Tabs >
            <Tab heading="Tab1" tabStyle={{...styles.tabStyles}}  
            activeTabStyle={{...styles.activeTabStyles}} activeTextStyle={{...styles.activeTextStyle}}>
                <Text>A</Text>
                
            </Tab>
            <Tab heading="Tab2" tabStyle={{...styles.tabStyles}}  
            activeTabStyle={{...styles.activeTabStyles}} activeTextStyle={{...styles.activeTextStyle}}>
                <Text>B</Text>
                
            </Tab>
        </Tabs> 
    )
}



