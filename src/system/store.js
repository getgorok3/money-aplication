import { createStore } from 'redux';
import { combineReducers } from 'redux'
import walletReducer from '../reducers/walletReducer'

export const  store = createStore(combineReducers({ wallet: walletReducer }));