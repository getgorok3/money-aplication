import {
    ADD_WALLET,
    EDIT_WALLET,
    DELETE_WALLET,
    GET_WALLET,
    GET_WALLET_LIST
} from '../constants/appContants'


export const addWallet = ({ name, money, income, expense }) => ({
    type: ADD_WALLET,
    payload: { name, money, income, expense },
})

export const editWallet = (index, { name, money, income, expense,id }) => ({
    type: EDIT_WALLET,
    payload: { index, name, money, income, expense,id },
})

export const deleteWallet = (index) => ({
    type: DELETE_WALLET,
    payload: { index },
})

export const getWallet = (index) => ({
    type: GET_WALLET,
    payload: { index },
})

export const getWalletList = () => ({
    type: GET_WALLET_LIST,
})




