import { ADD_WALLET, EDIT_WALLET , DELETE_WALLET, GET_WALLET, GET_WALLET_LIST} from "../constants/appContants";

const addWallet = (state, payload) =>{
    const arr = state.map(item => item)                             // init new array then push new obj
    arr.push(payload)
    return arr
}

const editWallet = (state, { index, ...data}) =>{
    return state.map((item, targetIndex) => {                   
        targetIndex === index ? data : item                         // find index first then replace its
    })
}

const getWallet = (state, {index}) =>{
    return state.map((item, targetIndex) => {                   
        targetIndex === index ? item : {}                         // find index first then replace its
    })
}


const getWalletList = (state) =>{
    if(!!state.wallet && state.wallet.length > 0){
        return state.wallet
    }else{
        return []
    }
}

const deleteWallet = (state, {index}) =>{
    return state.filter((item, targetIndex) => targetIndex !== index)      // array without selected item
}

export default (state = [], { type, payload }) => {
    switch (type) {
        case ADD_WALLET:
            return addWallet(state, payload)
        case EDIT_WALLET:
            return editWallet(state, payload)
        case DELETE_WALLET:
            return deleteWallet(state, payload)
        case GET_WALLET:
            return getWallet(state, payload)
        case GET_WALLET_LIST:
            return getWalletList(state)
        default:
            return state
    }

}